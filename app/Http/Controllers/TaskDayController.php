<?php

namespace App\Http\Controllers;

use App\task_day;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requet = $request->all();

        $task = new task_day;
        $task->name = $request->nombre;
        $task->user = Auth::id();
        $task->date = date('Y-m-d H:i:s');
        $task->save();

        return redirect()->route('task.index')->with('success','Empresa registrada con éxito'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\task_day  $task_day
     * @return \Illuminate\Http\Response
     */
    public function show(task_day $task_day)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\task_day  $task_day
     * @return \Illuminate\Http\Response
     */
    public function edit(task_day $task_day)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\task_day  $task_day
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, task_day $task_day)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\task_day  $task_day
     * @return \Illuminate\Http\Response
     */
    public function destroy(task_day $task_day)
    {
        $task_day->delete();
        return ('Tarea borrada con éxito');
    }

    public function nueva($nombre){
        return 'Buena';
    }

}
