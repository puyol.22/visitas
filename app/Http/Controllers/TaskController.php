<?php

namespace App\Http\Controllers;

use App\task;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\task_day;
use App\Auth;
use App\statuscompany;
use App\User;
use App\company;
use App\visits_planing_day;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listTask = task::all();
        return view('system.task.index', ['listTask' => $listTask]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('system.task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requet = $request->all();

        $task = new task;
        $task->name = $request->nombre;
        $task->status = $request->estado;
        $task->save();

        return redirect()->route('taskIndex')->with('success','Empresa registrada con éxito'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(task $task)
    {
        $listStatus = statuscompany::all();
        $listCompany = company::all();
        $listUsers = User::all();
        $listTask  = task::all();
        $listTaskDay = task_day::all();
        return view('system.task.show',compact([
            'task', 
            'listStatus',
            'listCompany',
            'listUsers',
            'listTask',
            'listTaskDay'
            ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(task $task)
    {
        $listStatus = statuscompany::all();
        $listCompany = company::all();
        $listUsers = User::all();
        $listTask  = task::all();
        $listTaskDay = task_day::all();
        return view('system.task.edit',compact([
            'task', 
            'listStatus',
            'listCompany',
            'listUsers',
            'listTask',
            'listTaskDay'
            ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, task $task)
    {
        $requet = $request->all();
        $numTask = task::where('id', '=', $request->id)->count();        

        if($numTask >=1){
            $task_ = task::where('id', '=', $request->id)->first();
            $task_->name = $request->nombre;
            $task_->status = $request->estado;
            $task_->save();
         }
         

        return redirect()->route('taskIndex')->with('success','Tarea editada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(task $task)
    {
        $task->delete();
        return redirect()->route('taskIndex')->with('success','Tarea borrada con éxito');
    }

    public function borrarTareaDay(Request $request, task_day $task_day)
    {
        if($request->ajax())
        {
            $task_Day = task_day::where('id', '=', $request->id)->first();
            $task_Day->delete();
            $user = auth()->user();
            return task_day::where('user','=',$user->id)->get();
        }else{
            $task_Day = task_day::where('id', '=', $request->id)->first();
            $task_Day->delete();

            return redirect()->route('visitsIndex')->with('success','Tarea borrada con éxito');
        }       
    }

    public function nueva($id){        
        $replicaTask = task::where('id','=',$id)->get();
        $user = auth()->user();

        $nuevaTask = new task_day;
        $nuevaTask->name = $replicaTask[0]->name;
        $nuevaTask->user = $user->id;
        $nuevaTask->date = date('Y-m-d H:i:s');
        $nuevaTask->id_task = $id;
        $nuevaTask->save();

        return task_day::where('user','=',$user->id)->get();
    }

    public function borrar(Request $request, $id){
        if($request->ajax()){
            $borrarTask = task_day::where('id','=',$id);
            $borrarTask->delete();        
            $user = auth()->user();
            return task_day::where('user','=',$user->id)->get();
        }else{
            $borrarTask = task_day::where('id','=',$id);
            $borrarTask->delete();
            return redirect()->route('visitsIndex')->with('success','Tarea borrada con éxito');
        }
    }

    public function borrarSucursal(Request $request, $id){
        if($request->ajax())
        {
            $borrarTask = visits_planing_day::where('id','=',$id)->first();
            $borrarTask->delete();        
            $user = auth()->user();
            return visits_planing_day::where('user','=',$user->id)->get();
        }else{
            $borrarTask = visits_planing_day::where('id','=',$id)->first();
            $borrarTask->delete(); 
            return redirect()->route('visitsIndex')->with('success','Tarea borrada con éxito');   
        }
    }

}
