<?php

namespace App\Http\Controllers;

use App\company;
use App\Http\Controllers\Controller;
use App\statuscompany;
use App\task;
use App\task_day;
use App\User;
use Illuminate\Http\Request;

class CompanyController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$listCompany = company::all();
		return view('system.company.index', ['listCompany' => $listCompany]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('system.company.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$requet = $request->all();

		$company = new company;
		$company->rut = $request->rut;
		$company->name = $request->name;
		$company->phone = $request->telefono;
		$company->address = $request->direccion;
		$company->status = $request->estado;

		$company->save();

		return redirect()->route('companyIndex')->with('success', 'Empresa registrada con éxito');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\company  $company
	 * @return \Illuminate\Http\Response
	 */
	public function show(company $company) {
		$listStatus = statuscompany::all();
		$listCompany = company::all();
		$listUsers = User::all();
		$listTask = task::all();
		$listTaskDay = task_day::all();
		return view('system.company.show', compact([
			'company',
			'listStatus',
			'listCompany',
			'listUsers',
			'listTask',
			'listTaskDay',
		]));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\company  $company
	 * @return \Illuminate\Http\Response
	 */
	public function edit(company $company) {
		$listStatus = statuscompany::all();
		$listCompany = company::all();
		$listUsers = User::all();
		$listTask = task::all();
		$listTaskDay = task_day::all();
		return view('system.company.edit', compact([
			'company',
			'listStatus',
			'listCompany',
			'listUsers',
			'listTask',
			'listTaskDay',
		]));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\company  $company
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, company $company) {
		$requet = $request->all();
		$numCompany = company::where('id', '=', $request->id)->count();

		if ($numCompany >= 1) {
			$compa = company::where('id', '=', $request->id)->first();
			$compa->rut = $request->rut;
			$compa->name = $request->nombre;
			$compa->phone = $request->telefono;
			$compa->address = $request->direccion;
			$compa->status = $request->estado;
			$compa->save();
		}

		return redirect()->route('companyIndex')->with('success', 'Empresa editada con éxito');
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(company $company)
    {
        $company->delete();
        return redirect()->route('companyIndex')->with('success','Empresa borrada con éxito');
    }
    
}
