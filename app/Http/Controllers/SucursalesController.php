<?php

namespace App\Http\Controllers;

use App\sucursales;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\statuscompany;
use App\company;
use App\User;
use App\task_day;

class SucursalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listSucursales = sucursales::all();
        $listStatus = statuscompany::all();
        $listCompany = company::all();
        return view('system.sucursales.index', [
                                'listSucursales' => $listSucursales,
                                'listStatus'     => $listStatus,
                                'listCompany' => $listCompany
                                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $requet = $request->all();
        var_dump('datos: '.$request->estado); exit;
        $sucursal = new sucursales;
        $sucursal->rut = $request->rut;
        $sucursal->name = $request->name;
        $sucursal->phone = $request->telefono;
        $sucursal->address = $request->direccion;
        $sucursal->status = $request->estado;
        
        $sucursal->save();

        return redirect()->route('sucursales.index')->with('success','Sucursal registrada con éxito'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sucursales  $sucursales
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, sucursales $sucursales)
    {        
        $idSucursal = $request->route()->sucursale;
        $listCompany = company::all();
        $listStatus = statuscompany::all();
        $sucursal = sucursales::where('id', '=', $idSucursal)->first();
        $listUsers = User::all();
        $listTaskDay = task_day::all();        

        return view('system.sucursales.show', [
            'sucursal' => $sucursal,
            'listStatus' => $listStatus,
            'listCompany' => $listCompany
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sucursales  $sucursales
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, sucursales $sucursales)
    {
        $idSucursal = $request->route()->sucursale;
        $sucursal = sucursales::where('id', '=', $idSucursal)->first();
        $listStatus = statuscompany::all();
        $listCompany = company::all();
        $listUsers = User::all();
        $listTaskDay = task_day::all();

        return view('system.sucursales.edit',(['sucursal' => $sucursal, 'listStatus' => $listStatus, 'listCompany' => $listCompany]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sucursales  $sucursales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sucursales $sucursales)
    {
        $requet = $request->all();
        $numCompany = sucursales::where('id', '=', $request->id)->count();        

        if($numCompany >=1){
            $sucursa = sucursales::where('id', '=', $request->id)->first();
            $sucursa->rut = $request->rut;
            $sucursa->name = $request->nombre;
            $sucursa->phone = $request->telefono;
            $sucursa->address = $request->direccion;
            $sucursa->status = $request->estado;
            $sucursa->company_id = $request->empresa;
            $sucursa->save();
         }
         

        return redirect()->route('sucursales.index')->with('success','Sucursal editada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sucursales  $sucursales
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, sucursales $sucursales)
    {
        $idSucursal = $request->route()->sucursale;
        $sucursal = sucursales::find($idSucursal);        
        $sucursal->delete();
        return redirect()->route('sucursales.index')->with('success','Sucursal borrada con éxito');
    }
}
