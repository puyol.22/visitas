<?php

namespace App\Http\Controllers;

use App\clients;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\statuscompany;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listUsers = User::all();
        $listStatus = statuscompany::all();
        return view('system.client.index', ['listUsers' => $listUsers, 'listStatus' => $listStatus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('system.client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requet = $request->all();

        $clients = new clients;
        $clients->rut = $request->rut;
        $clients->name = $request->name;
        $clients->progress = 1;
        $clients->company = 1;
        $clients->city = 1;
        $clients->save();

        return redirect()->route('client.index')->with('success','Empresa registrada con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, clients $clients)
    {
        $idUser = $request->route()->client;
        $listStatus = statuscompany::all();
        $usuario = User::where('id', '=', $idUser)->first();
        $listUsers = User::all();

        return view('system.client.show', [
            'usuario' => $usuario,
            'listStatus' => $listStatus
            ]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, clients $clients)
    {
        $idUser = $request->route()->client;
        $listStatus = statuscompany::all();
        $usuario = User::where('id', '=', $idUser)->first();

        return view('system.client.edit',(['usuario' => $usuario, 'listStatus' => $listStatus]));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, clients $clients)
    {
        $requet = $request->all();
        $numCompany = User::where('id', '=', $request->id)->count();        

        if($numCompany >=1){
            $client = User::where('id', '=', $request->id)->first();
            $client->name = $request->nombre;
            $client->email = $request->email;
            $client->status = $request->estado;
            $client->save();
         }
         

        return redirect()->route('clients.index')->with('success','Usuario editado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, clients $clients)
    {
        $idUser = $request->route()->client;
        $usuario = User::find($idUser);        
        $usuario->delete();
        return redirect()->route('clients.index')->with('success','Usuario borrado con éxito');
    }
}
