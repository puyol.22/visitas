<?php

namespace App\Http\Controllers;

use App\failed_jobs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FailedJobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\failed_jobs  $failed_jobs
     * @return \Illuminate\Http\Response
     */
    public function show(failed_jobs $failed_jobs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\failed_jobs  $failed_jobs
     * @return \Illuminate\Http\Response
     */
    public function edit(failed_jobs $failed_jobs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\failed_jobs  $failed_jobs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, failed_jobs $failed_jobs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\failed_jobs  $failed_jobs
     * @return \Illuminate\Http\Response
     */
    public function destroy(failed_jobs $failed_jobs)
    {
        //
    }
}
