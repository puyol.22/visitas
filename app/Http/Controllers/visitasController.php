<?php

namespace App\Http\Controllers;

use App\branch_office;
use App\company;
use App\Http\Controllers\Controller;
use App\task;
use App\task_day;
use App\User;
use App\visitas;
use App\visits_planing_day;
use App\visits_planning;
use Illuminate\Http\Request;
use App\statuscompany;
use Illuminate\Support\Facades\Auth;

class visitasController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
		$arrayVisitas = [];
		$listCompany = company::all();
		$lsc = new branch_office;
		$listSucursales = $lsc::all();
		$listUsers = User::all();
		$listTask = task::where('status', '=', 5)->get();
		$userId = Auth::id();
		$listTaskDay = task_day::where('user', '=', $userId)->get();
		$visits_planing_day = new visits_planing_day();
		$listadoSucursales = $visits_planing_day::where('user', '=', $userId)->get();
		$vs = new visits_planning();
		$visitas = $vs->select('id as id', 'date_visit as start', 'date_visit as end')->where('user_id', '=', $userId)->get();

		foreach ($visitas as $key => $value) {
			$value->title = 'Visita';
			$value->backgroundColor = '#e0e4f4';
			$value->borderColor = '#00cccc';
			$value->description = 'visita agendada';
		}

		//dd(json_encode($arrayVisitas));
		return View('system/visits/index', [
			'listCompany' => $listCompany,
			'listSucursales' => $listSucursales,
			'listUsers' => $listUsers,
			'listTask' => $listTask,
			'listTaskDay' => $listTaskDay,
			'listadoSucursales' => $listadoSucursales,
			'visitas' => $visitas->toJson(),
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\visitas  $visitas
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request) {

		$userId = Auth::id();

		$visitas = visits_planning::where('user_id', '=', $userId)->get();
		return $visitas->toJson();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\visitas  $visitas
	 * @return \Illuminate\Http\Response
	 */
	public function edit(visitas $visitas) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\visitas  $visitas
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, visitas $visitas) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\visitas  $visitas
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(visitas $visitas) {
		//
	}

	public function borrarVisita(Request $request, visitas $visitas){
		$idVisita = $request->route()->id;
		$visita = visits_planning::where('id', '=', $idVisita)->first();
		$visita->delete();
        return redirect()->route('aprobador')->with('success','Visita borrada con éxito');		
	}

	public function aprobarVisita(Request $request, visitas $visitas){
		$idVisita = $request->route()->id;
		$visita = visits_planning::where('id', '=', $idVisita)->first();
		$visita->execute = 2;
		$visita->save();
		return redirect()->route('aprobador')->with('success', 'Visita aprobada con éxito');
	}

	public function aprobador() {
		$arrayVisitas = [];
		$listStatus = statuscompany::all();
		$listCompany = company::all();
		$lsc = new branch_office;
		$listSucursales = $lsc::all();
		$listUsers = User::all();
		$listTask = task::where('status', '=', 5)->get();
		$userId = Auth::id();
		$listTaskDay = task_day::where('user', '=', $userId)->get();
		$visits_planing_day = new visits_planing_day();
		$listadoSucursales = $visits_planing_day::where('user', '=', $userId)->get();
		$vs = new visits_planning();
		$visitas = $vs->select('id as id', 'date_visit as start', 'date_visit as end')->where('user_id', '=', $userId)->get();
		$listVisitasxAprobar = visits_planning::where('execute','=',1)
								->where('status','=',1)->get();

		return view('system/aprobador/index',[
			'listCompany' => $listCompany,
			'listSucursales' => $listSucursales,
			'listUsers' => $listUsers,
			'listTask' => $listTask,
			'listTaskDay' => $listTaskDay,
			'listadoSucursales' => $listadoSucursales,
			'listStatus' => $listStatus,
			'visitas' => $visitas->toJson(),
			'listVisitasxAprobar' => $listVisitasxAprobar
		]);
	}
}
