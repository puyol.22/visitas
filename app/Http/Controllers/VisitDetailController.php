<?php

namespace App\Http\Controllers;

use App\visit_detail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VisitDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\visit_detail  $visit_detail
     * @return \Illuminate\Http\Response
     */
    public function show(visit_detail $visit_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\visit_detail  $visit_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(visit_detail $visit_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\visit_detail  $visit_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, visit_detail $visit_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\visit_detail  $visit_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(visit_detail $visit_detail)
    {
        //
    }
}
