<?php

namespace App\Http\Controllers;

use App\visits_planning;
use App\visits_planing_day;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\company;
use App\User;
use App\task;
use App\task_day;
use Auth;
use App\visit_detail;
use App\branch_office;

// $listadoSucursales = array();

class VisitsPlaningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = 0;
        
        if($request->fecha == null) $error = 1;
        if($request->visitador == "-1") $error = 1;

        $userId = Auth::id();
        $cantSucursales = visits_planing_day::where('user','=',$userId)->count();
        
        if($error == 0 && $cantSucursales > 0){
            
            //guardar visita
            $listasucursale = visits_planing_day::where('user','=',$userId)->get();
            foreach($listasucursale as $sucursal)
            {
                $getCompanyBySucursal = branch_office::where('id','=',$sucursal->idsucursal)->get();
                foreach($getCompanyBySucursal as $getCompanyId)
                {
                    $idCompany = $getCompanyId->company_id;
                }

                $visita = new visits_planning;
                $visita->user_id = $request->visitador;
                $visita->company_id = $idCompany;
                $visita->branchoffice_id = $sucursal->idsucursal;
                $visita->date_planing = $request->fecha;
                $visita->date_visit = $request->fecha;
                $visita->execute = 1;
                $visita->status = 1;
                $visita->observations;
                $visita->save();

                $lastid = $visita->id;

                //guardar detalle visita
                $taskDay = task_day::where('user','=',$userId)->get();
                foreach($taskDay as $task){
                    $visita_detalle = new visit_detail;
                    $visita_detalle->visits_planing = $lastid;
                    $visita_detalle->id_task = $task->id_task;
                    $visita_detalle->status = 1;
                    $visita_detalle->save();
                }
            }
            

            //limpiar tablas de configuracion
            $taskDayBorrar = task_day::where('user','=',$userId)->get();
            foreach($taskDayBorrar as $tasborrar){
                $taskABorrar = task_day::where('id','=',$tasborrar->id)->delete();
            }
            $listsucursalesborrar = visits_planing_day::where('user','=',$userId)->get();
            foreach($listsucursalesborrar as $sucursalborrar)
            {
                $sucursalABorrar = visits_planing_day::where('id','=',$sucursalborrar->id)->delete(); 
            }
            
            $listCompany  = company::all();
            $listUsers = User::all();
            $listTask = task::all();
            $listTaskDay = task_day::all();

            return View('/home', [
                            'listCompany' => $listCompany,
                            'listUsers'   => $listUsers,
                            'listTask'    => $listTask,
                            'listTaskDay' => $listTaskDay,
                            'message'     => 'Visita por aprobar !!!'
            ]);

        }else{
            $listCompany  = company::all();
            $listUsers = User::all();
            $listTask = task::all();
            $listTaskDay = task_day::all();

            return View('/home', [
                            'listCompany' => $listCompany,
                            'listUsers'   => $listUsers,
                            'listTask'    => $listTask,
                            'listTaskDay' => $listTaskDay,
                            'message'     => 'La Visita no pudo ser guardada, al parecer hay error en datos ingresados'
            ]);
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\visits_planning  $visits_planning
     * @return \Illuminate\Http\Response
     */
    public function show(visits_planning $visits_planning)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\visits_planning  $visits_planning
     * @return \Illuminate\Http\Response
     */
    public function edit(visits_planning $visits_planning)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\visits_planning  $visits_planning
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, visits_planning $visits_planning)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\visits_planning  $visits_planning
     * @return \Illuminate\Http\Response
     */
    public function destroy(visits_planning $visits_planning)
    {
        //
    }

    public function nueva($id){
        $user = auth()->user();
        
        $sucursal = branch_office::where('id','=',$id)->get();

        $visits_planing_day = new visits_planing_day;
        $visits_planing_day->namesucursal = $sucursal[0]->name;
        $visits_planing_day->user = $user->id;
        $visits_planing_day->idsucursal = $sucursal[0]->id;
        $visits_planing_day->save();
        
        return visits_planing_day::where('user','=',$user->id)->get();
    }

    public function borrar($id){        
        $borrarTask = visits_planing_day::where('id','=',$id);
        $borrarTask->delete();        
        $user = auth()->user();
        return visits_planing_day::where('user','=',$user->id)->get();
    }

}
