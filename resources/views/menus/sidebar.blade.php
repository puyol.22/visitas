<div class="az-sidebar-body">
        <ul class="nav">
          <li class="nav-label">Menu</li>
         <!--  <li class="nav-item ">
            <a href="" class="nav-link with-sub"><i class="typcn typcn-clipboard"></i>Eventos</a>
            <ul class="nav-sub">
              <li class="nav-sub-item"><a href="{{ route('eventIndex') }}" class="nav-sub-link">Cronograma</a></li>
            </ul>
          </li>nav-item -->
          <li class="nav-item">
            <a href="" class="nav-link with-sub"><i class="typcn typcn-document"></i>Visitas</a>
            <ul class="nav-sub">
              <li class="nav-sub-item">
                <a href="{{ route('visitsIndex') }}" class="nav-sub-link">Calendario</a>
              </li>
               <li class="nav-sub-item">
                <a href="{{ route('aprobador') }}" class="nav-sub-link">Aprobador</a>
              </li>
            </ul>
          </li><!-- nav-item -->
          <li class="nav-item">
            <a href="" class="nav-link with-sub"><i class="typcn typcn-book"></i>Reportes</a>
            <ul class="nav-sub">
              <li class="nav-sub-item"><a href="{{ route('eventIndex') }}" class="nav-sub-link">Eventos</a></li>
              <li class="nav-sub-item"><a href="{{ route('eventList') }}" class="nav-sub-link">Visitas</a></li>
            </ul>
          </li><!-- nav-item -->
          <li class="nav-item">
            <a href="" class="nav-link with-sub"><i class="typcn typcn-book"></i>Mantenedor</a>
            <ul class="nav-sub">
              <li class="nav-sub-item"><a href="{{ route('companyIndex') }}" class="nav-sub-link">Empresas</a></li>
              <li class="nav-sub-item"><a href="{{ route('sucursales.index') }}" class="nav-sub-link">Sucursales</a></li>
              <li class="nav-sub-item"><a href="{{ route('taskIndex') }}" class="nav-sub-link">Tareas</a></li>
              <li class="nav-sub-item"><a href="{{ route('clients.index') }}" class="nav-sub-link">Usuarios</a></li>

            </ul>
          </li><!-- nav-item -->
        </ul><!-- nav -->
      </div><!-- az-sidebar-body -->