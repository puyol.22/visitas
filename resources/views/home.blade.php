
@extends('layouts.home')



@section('content')

  @include('menus.locate_header',['some' => 'Hola, Bienvenido!','location'=>'Tablero de Gestion de Visitas.'])

  
  <div class="alert alert-warning" role="alert">
    {{ $message ?? '' }}
  </div>
  

  <div class="az-content-body">
        <div class="row row-sm">
          <div class="col-sm-6 col-lg-4 col-xl-3">
            <div class="card card-body card-dashboard-fifteen">
              <h1>257</h1>
              <label class="tx-purple">Visitas Agendadas</label>
              <span>N° de Visitas Totales.</span>
              <div class="chart-wrapper">
                <div id="flotChart1" class="flot-chart"></div>
              </div><!-- chart-wrapper -->
            </div><!-- card -->
          </div><!-- col -->
          <div class="col-sm-6 col-lg-4 col-xl-3 mg-t-20 mg-sm-t-0">
            <div class="card card-body card-dashboard-fifteen">
              <h1>187</h1>
              <label class="tx-primary">Visitas Realizadas</label>
              <span>Visitas Cerradas en campo </span>
              <div class="chart-wrapper">
                <div id="flotChart2" class="flot-chart"></div>
              </div><!-- chart-wrapper -->
            </div><!-- card -->
          </div><!-- col -->
          <div class="col-sm-6 col-lg-4 col-xl-3 mg-t-20 mg-sm-t-20 mg-lg-t-0">
            <div class="card card-body card-dashboard-fifteen">
              <h1>125<span>/187</span></h1>
              <label class="tx-teal">Visitas Pendientes</label>
              <span>Aun por Realizar.</span>
              <div class="chart-wrapper">
                <div id="flotChart3" class="flot-chart"></div>
              </div><!-- chart-wrapper -->
            </div><!-- card -->
          </div><!-- col -->
          <div class="col-sm-6 col-lg-12 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="d-lg-flex d-xl-block">
              <div class="card wd-lg-50p wd-xl-auto">
                <div class="card-header">
                  <h6 class="card-title tx-14 mg-b-0">Tiempo para la proxima cita</h6>
                </div><!-- card-header -->
                <div class="card-body">
                  <h3 class="tx-bold tx-inverse lh--5 mg-b-15">7m:32s <span class="tx-base tx-normal tx-gray-600">/ Goal: 8m:0s</span></h3>
                  <div class="progress mg-b-0 ht-3">
                    <div class="progress-bar wd-85p bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div><!-- card-body -->
              </div><!-- card -->
              <div class="card mg-t-20 mg-lg-t-0 mg-xl-t-20 mg-lg-l-20 mg-xl-l-0">
                <div class="card-header">
                  <h6 class="card-title tx-14 mg-b-5">Proxima Cita:</h6>
                  <p class="tx-12 lh-4 tx-gray-500 mg-b-0">Tienda Paris Sucursal Ahumada. / Ahumada 178 / Horario: 11:00 - 20:00 hrs</p>
                </div><!-- card-header -->
                <div class="card-body">
                  <h2 class="tx-bold tx-inverse lh--5 mg-b-5">12:h:30m</h2>
                </div><!-- card-body -->
              </div><!-- card -->
            </div>
          </div>

          <div class="col-gl-5 col-xl-6 mg-t-20">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title tx-14 mg-b-5">Monitor de Visitas Por Realizar</h6>
                <p class="mg-b-0">Status de Visitas... <a href="">Lista completa</a></p>
              </div><!-- card-header -->
              <div class="table-responsive mg-t-15">
                <table class="table table-striped table-talk-time">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Cliente</th>
                      <th>Direccion</th>
                      <th>Fecha</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>21</td>
                      <td>Paris</td>
                      <td>Av Libertador Bernardo O'Higgins 815</td>
                      <th>25/12/2019:12:30</th>
                    </tr>
                    <tr>
                      <td>22</td>
                      <td>Paris</td>
                      <td>Ahumada 178</td>
                      <th>25/12/2019:13:30</th>
                    </tr>
                    <tr>
                      <td>23</td>
                      <td>Santa Isabel</td>
                      <td>Av. Bernardo O’higgins 1449, Santiago Centro</td>
                      <th>25/12/2019:14:30</th>
                    </tr>
                    <tr>
                      <td>24</td>
                      <td>Easy</td>
                      <td>Av Presidente Kennedy 9001, 1132, Las Condes</td>
                      <th>25/12/2019:17:30</th>
                    </tr>
                    <tr>
                      <td>25</td>
                      <td>Santa Isabel</td>
                      <td>Bandera 201, Santiago Centro</td>
                      <th>26/12/2019:12:30</th>
                    </tr>
                    <tr>
                      <td>25</td>
                      <td>jumbo</td>
                      <td>Av Francisco Bilbao 4144, Las Condes.</td>
                      <th>26/12/2019:14:30</th>
                    </tr>
                  </tbody>
                </table>
              </div><!-- table-responsive -->
            </div><!-- card -->
          </div><!-- col -->
          <div class="col-md-5 col-lg-5 col-xl-4 mg-t-20">
            <div class="card card-dashboard-sixteen">
              <div class="card-header">
                <h6 class="card-title tx-14 mg-b-0">Vendedores</h6>
              </div><!-- card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table mg-b-0">
                    <tbody>
                      <tr>
                        <td>
                          <div class="az-img-user"><img src="https://via.placeholder.com/500" alt=""></div>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">Pedro Perez</h6>
                          <small class="tx-11 tx-gray-500">Agente ID: 12022</small>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">87/100</h6>
                          <small class="tx-11 tx-gray-500">Visitas</small>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="az-img-user"><img src="https://via.placeholder.com/500" alt=""></div>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">Reynante Labares</h6>
                          <small class="tx-11 tx-gray-500">Agente ID: 12028</small>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">85/100</h6>
                          <small class="tx-11 tx-gray-500">Visitas</small>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="az-img-user"><img src="https://via.placeholder.com/500" alt=""></div>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">Owen Bongcaras</h6>
                          <small class="tx-11 tx-gray-500">Agent ID: 11500</small>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">83/100</h6>
                          <small class="tx-11 tx-gray-500">Visitas</small>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="az-img-user"><img src="https://via.placeholder.com/500" alt=""></div>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">Mariane Galeon</h6>
                          <small class="tx-11 tx-gray-500">Agent ID: 11600</small>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">82/100</h6>
                          <small class="tx-11 tx-gray-500">Visitas</small>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="az-img-user"><img src="https://via.placeholder.com/500" alt=""></div>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">Joyce Chua</h6>
                          <small class="tx-11 tx-gray-500">Agent ID: 11990</small>
                        </td>
                        <td>
                          <h6 class="mg-b-0 tx-inverse">80/100</h6>
                          <small class="tx-11 tx-gray-500">Visitas</small>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div><!-- table-responsive -->
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col -->
          <div class="col-md-7 col-lg-7 col-xl-8 mg-t-20">
            <div class="card">
              <div class="card-header">
                <h6 class="card-title tx-14 mg-b-5">Customer Complaints Comparison</h6>
                <p class="mg-b-0">Monitor the total number of complaints that are resolved and unresolved.</p>
              </div><!-- card-header -->
              <div class="card-body">
                <div class="dashboard-five-stacked-chart"><canvas id="chartStacked1"></canvas></div>
              </div><!-- card-body -->
            </div><!-- card -->
          </div><!-- col -->
        </div><!-- row -->
      </div><!-- az-content-body -->

@endsection

