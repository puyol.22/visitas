
@extends('layouts.systema')



@section('content')

  @include('menus.locate_header',['some' => 'Sucursales','location'=>'Home/Sucursales'])

  <!-- <company></company> -->

  <hr>
<div class="row">
    <div class="col-lg-12">
        <h2 class="text-center">Listado de Sucursales</h2>
    </div>
    <div class="col-lg-12 text-center" style="margin-top:10px;margin-bottom: 10px;">
        <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#createSucursal">
            Agregar Sucursal
        </a><br><br>
    </div>
</div>

<div class="modal fade" id="createSucursal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>
                <h4>Agregar Sucursal</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('sucursales.store') }}" method="POST">
                @csrf

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Empresa:</strong>
                            <select class="browser-default custom-select" name="empresa" id="empresa">
                                <option selected>Seleccione un Estado ...</option>
                                @foreach ($listCompany as $empresa)
                                    <option value="{{$empresa->id}}">{{ $empresa->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            <input type="text" name="nombre" class="form-control" placeholder="Ingrese el nombre de la sucursal">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Estatus:</strong>
                            <select class="browser-default custom-select" name="estado" id="estado">
                                <option selected>Seleccione un Estado ...</option>
                                @foreach ($listStatus as $Status)
                                    <option value="{{$Status->id}}">{{ $Status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Rut:</strong>
                            <input type="text" name="rut" class="form-control" placeholder="Ingrese el rut de la sucursal">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Telefono:</strong>
                            <input type="text" name="telefono" id="telefono" class="form-control" placeholder="Ingrese el telefono de la sucursal">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Dirección:</strong>
                            <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Ingrese la dirección de la sucursal">
                        </div>
                    </div>
                </div>

            </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success btn-submit" value="Guardar">
            </div>
        </div>
    </div>
</div>

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Nombre</th>
        <th>Estado</th>
        <th>Rut</th>
        <th>Telefono</th>
        <th>Direccion</th>
        <th>Empresa</th>
        <th width="280px">Opciones</th>
    </tr>
    @foreach ($listSucursales as $sucursal)
        <tr>
            <td>{{ $sucursal->id }}</td>
            <td>{{ $sucursal->name }}</td>
            <td>
                <select class="browser-default custom-select" name="estado" id="estado">
                    <option selected>Seleccione un Estado ...</option>
                    @foreach ($listStatus as $Status)
                        <option value="{{$Status->id}}" {{ $Status->id == $sucursal->status? 'selected' : '' }}>{{ $Status->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>{{ $sucursal->rut }}</td>
            <td>{{ $sucursal->phone }}</td>
            <td>{{ $sucursal->address }}</td>
            <td>
                <select class="browser-default custom-select" name="empresa" id="empresa">
                    <option selected>Seleccione una empresa ...</option>
                    @foreach ($listCompany as $empresa)
                        <option value="{{$empresa->id}}" {{ $empresa->id == $sucursal->company_id? 'selected' : '' }}>{{ $empresa->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <form action="{{ route('sucursales.destroy',$sucursal->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('sucursales.show',$sucursal->id) }}">Ver</a>
                    <a class="btn btn-primary" href="{{ route('sucursales.edit',$sucursal->id) }}">Editar</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>


@endsection

@section('jsInclude')
 <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('lib/ionicons/ionicons.js')}}"></script>
    <script src="{{asset('lib/jquery.flot/jquery.flot.js')}}"></script>
    <script src="{{asset('lib/jquery.flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('lib/jquery.flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('lib/chart.js/Chart.bundle.min.js')}}"></script>

    <script src="{{asset('js/azia.js')}}"></script>

<script>

      $(function(){
        'use strict'

        $('.az-sidebar .with-sub').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('show');
          $(this).parent().siblings().removeClass('show');
        })

        $(document).on('click touchstart', function(e){
          e.stopPropagation();

          // closing of sidebar menu when clicking outside of it
          if(!$(e.target).closest('.az-header-menu-icon').length) {
            var sidebarTarg = $(e.target).closest('.az-sidebar').length;
            if(!sidebarTarg) {
              $('body').removeClass('az-sidebar-show');
            }
          }
        });

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $(".btn-submit").click(function(e){
          e.preventDefault();
          var nombre = $("input[name=nombre]").val();
          var token = $('meta[name="csrf-token"]').attr('content')
          var estado = $('#estado').val();
          var empresa = $('#empresa').val();
          var rut = $("input[name=rut]").val();
          var telefono = $("input[name=telefono]").val();
          var direccion = $("input[name=direccion]").val();
          $.ajax({
            type:'POST',
            url:'/sucursales.store',
            data:{token:token, nombre:nombre, estado:estado, rut:rut, telefono:telefono, direccion:direccion, empresa:empresa},
            success:function(data){
              console.log(data);
              alert(data.message);
              window.location.href = "sucursales";
            }
            });
        });

        $('#azSidebarToggle').on('click', function(e){
          e.preventDefault();

          if(window.matchMedia('(min-width: 992px)').matches) {
            $('.az-sidebar').toggle();
          } else {
            $('body').toggleClass('az-sidebar-show');
          }
        })
      });

</script>
@endsection

