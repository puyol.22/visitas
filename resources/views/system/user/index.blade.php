@extends('layouts.app')

@section('content')


<hr>
<div class="row">
    <div class="col-lg-12">
        <h2 class="text-center">Listado de Usuarios</h2>
    </div>
</div>


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Rut</th>
        <th>Nombre</th>
        <th>Estado</th>
        <th width="280px">Opciones</th>
    </tr>
    @foreach ($listCompany as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->rut }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->status }}</td>
            <td>
                <form action="{{ route('user.destroy',$user->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('user.show',$user->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('user.edit',$user->id) }}">Edit</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>

<div class="col-lg-12 text-center" style="margin-top:10px;margin-bottom: 10px;">
    <a class="btn btn-success " href="{{ route('home') }}"> Inicio </a>
</div>

@endsection