
@extends('layouts.systema')



@section('content')

  @include('menus.locate_header',['some' => 'Usuario','location'=>'Home/Usuario'])

  <!-- <company></company> -->

  <hr>
<div class="row">
    <div class="col-lg-12">
        <h2 class="text-center">Listado de Usuarios</h2>
    </div>
    <div class="col-lg-12 text-center" style="margin-top:10px;margin-bottom: 10px;">
        <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#createUsuario">
            Agregar Usuario
        </a><br><br>
    </div>
</div>

<div class="modal fade" id="createUsuario">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>
                <h4>Agregar Usuario</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('sucursales.store') }}" method="POST">
                @csrf

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            <input type="text" name="nombre" class="form-control" placeholder="Ingrese el nombre del usuario">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Email:</strong>
                            <input type="text" name="email" class="form-control" placeholder="Ingrese el email del usuario">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Estatus:</strong>
                            <select class="browser-default custom-select" name="estado" id="estado">
                                <option selected>Seleccione un Estado ...</option>
                                @foreach ($listStatus as $Status)
                                    <option value="{{$Status->id}}">{{ $Status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>

            </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success btn-submit" value="Guardar">
            </div>
        </div>
    </div>
</div>

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Nombre</th>
        <th>Estado</th>
        <th>Email</th>
        <th width="280px">Opciones</th>
    </tr>
    @foreach ($listUsers as $usuario)
        <tr>
            <td>{{ $usuario->id }}</td>
            <td>{{ $usuario->name }}</td>
            <td>
                <select class="browser-default custom-select" name="estado" id="estado">
                    <option selected>Seleccione un Estado ...</option>
                    @foreach ($listStatus as $Status)
                        <option value="{{$Status->id}}" {{ $Status->id == $usuario->status? 'selected' : '' }}>{{ $Status->name }}</option>
                    @endforeach
                </select>
            </td>
            <td>{{ $usuario->email }}</td>
            <td>
                <form action="{{ route('clients.destroy',$usuario->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('clients.show',$usuario->id) }}">Ver</a>
                    <a class="btn btn-primary" href="{{ route('clients.edit',$usuario->id) }}">Editar</a>

                    @csrf
                    @method('DELETE')

                    <button type="submit" class="btn btn-danger">Borrar</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>


@endsection

@section('jsInclude')
 <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('lib/ionicons/ionicons.js')}}"></script>
    <script src="{{asset('lib/jquery.flot/jquery.flot.js')}}"></script>
    <script src="{{asset('lib/jquery.flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('lib/jquery.flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('lib/chart.js/Chart.bundle.min.js')}}"></script>

    <script src="{{asset('js/azia.js')}}"></script>

<script>

      $(function(){
        'use strict'

        $('.az-sidebar .with-sub').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('show');
          $(this).parent().siblings().removeClass('show');
        })

        $(document).on('click touchstart', function(e){
          e.stopPropagation();

          // closing of sidebar menu when clicking outside of it
          if(!$(e.target).closest('.az-header-menu-icon').length) {
            var sidebarTarg = $(e.target).closest('.az-sidebar').length;
            if(!sidebarTarg) {
              $('body').removeClass('az-sidebar-show');
            }
          }
        });

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $(".btn-submit").click(function(e){
          e.preventDefault();
          var nombre = $("input[name=nombre]").val();
          var token = $('meta[name="csrf-token"]').attr('content')
          var estado = $('#estado').val();
          var email = $("input[name=email]").val();

          $.ajax({
            type:'POST',
            url:'/clients.store',
            data:{token:token, nombre:nombre, estado:estado, email:email},
            success:function(data){
              console.log(data);
              alert(data.message);
              window.location.href = "clients";
            }
            });
        });

        $('#azSidebarToggle').on('click', function(e){
          e.preventDefault();

          if(window.matchMedia('(min-width: 992px)').matches) {
            $('.az-sidebar').toggle();
          } else {
            $('body').toggleClass('az-sidebar-show');
          }
        })
      });

</script>
@endsection

