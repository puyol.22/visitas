
@extends('layouts.calendar')


@section('content')

    @include('menus.locate_header',['some' => 'Visitas','location'=>'Home / Visitas','type_acction'=>'create'])
    <div class="az-content az-content-calendar">
      <div class="container">

        <div class="az-content-body az-content-body-calendar">

          <div id="calendar" class="az-calendar"></div>
        </div><!-- az-content-body -->
      </div><!-- container -->
    </div><!-- az-content -->
    <div class="modal fade" id="create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>×</span>
                </button>
                <h4>Agregar Visita</h4>
            </div>
            <div class="modal-body">
              <form action="{{ route('visitsPlanning.store') }}" method="POST">
                @csrf

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Empresa:</strong>
                            <select class="browser-default custom-select" name="company" id="company">
                                <option value="-1" selected>Seleccione una empresa ...</option>
                                @foreach ($listCompany as $company)
                                    <option value="{{$company->id}}">{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Sucursales:</strong>
                            <button type="button" class="btn btn-primary"  id="saveSucursal"> Agregar sucursal </button><br>
                            <select class="browser-default custom-select" name="sucursales" id="sucursales">
                                <option value="-1" selected>Seleccione un sucursal ...</option>
                            </select>
                        </div>
                        <strong>Listado de sucursales a visitar:</strong>
                            <table class="table" id="listadoSucursales" name="listadoSucursales">
                                <tbody>
                                    @foreach($listadoSucursales as $key => $sucursal)
                                    <tr>
                                        <th scope="row">{{ $sucursal->id }}</th>
                                        <td>{{ $sucursal->namesucursal }}</td>
                                        <td>
                                            <a href="/tareaBorrarSucursal/{{$sucursal->id}}" class="btn btn-warning" value="borrarsucursal">Borrar Sucursal</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Fecha de visita:</strong>
                            <input class="date form-control" type="text" id="fecha" name="fecha" placeholder="<?php echo Date("Y-m-d"); ?>">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Visitador:</strong>
                            <select class="browser-default custom-select" name="visitador" id="visitador">
                                <option value="-1" selected>Seleccione un usuario ...</option>
                                @foreach ($listUsers as $user)
                                    <option value="{{$user->id}}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Tareas a realizar en la visita:</strong>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <select class="browser-default custom-select" name="tareaNueva" id="tareaNueva">
                                    <option value="-1" selected>Seleccione un usuario ...</option>
                                    @foreach ($listTask as $task)
                                        <option value="{{$task->id}}">{{ $task->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="button" class="btn btn-primary" id="saveTarea"> Agregar tarea </button>
                            </div>
                            <br><strong>Listado de tareas a realizar en la visita:</strong>
                            <table class="table" id="listado" name="listado">
                                <tbody>
                                    @foreach($listTaskDay as $key => $taskday)
                                    <tr>
                                        <th scope="row">{{ $taskday->id }}</th>
                                        <td>{{ $taskday->name }}</td>
                                        <td>
                                            <a href="/task.destroy/{{$taskday->id}}" class="btn btn-warning" value="borrartarea">Borrar Tarea</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-submit" value="Guardar">
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('jsInclude')

  <script  src="{{asset('lib/jquery/jquery.min.js')}}"></script>
    <script defer src="{{asset('lib/moment/min/moment.min.js')}}"></script>
    <script defer src="{{asset('lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <script defer src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    {{-- <script defer src="{{asset('lib/ionicons/ionicons.js')}}"></script> --}}

    <script defer src="{{asset('lib/select2/js/select2.full.min.js')}}"></script>

    <script defer>


      $(function(){
        'use strict'
         $('.az-sidebar .with-sub').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('show');
          $(this).parent().siblings().removeClass('show');
        })

        $("body").delegate(".datepicker", "focusin", function(){
        $(this).datepicker();
    });

        $(document).on('click touchstart', function(e){
          e.stopPropagation();

          // closing of sidebar menu when clicking outside of it
          if(!$(e.target).closest('.az-header-menu-icon').length) {
            var sidebarTarg = $(e.target).closest('.az-sidebar').length;
            if(!sidebarTarg) {
              $('body').removeClass('az-sidebar-show');
            }
          }
        });


        $('#azSidebarToggle').on('click', function(e){
          e.preventDefault();

          if(window.matchMedia('(min-width: 992px)').matches) {
            $('.az-sidebar').toggle();
          } else {
            $('body').toggleClass('az-sidebar-show');
          }
        });

        $("#company").on('change', function(e){
            var company = $('#company').val();
            $.get('sucursalesByCompany/'+company, function(data){
                console.log(data);
                var sucursal = '<option value="">Seleccione una sucursal</option>'
                    for (var i=0; i<data.length;i++)
                    sucursal+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                    $("#sucursales").html(sucursal);
            });
        });

        $("#saveSucursal").on('click', function(e){
          e.preventDefault();
          var token = $('meta[name="csrf-token"]').attr('content')
          var tareaNueva = $('#sucursales').val();
          $.get('sucursalNueva/'+tareaNueva, function(data){
            // console.log(data);
            var tablalistado = '<table class="table" id="listadoSucursales" name="listadoSucursales"><tbody>'
                for(var j=0; j<data.length; j++){
                    tablalistado+='<tr><th scope="row">'+data[j].id+'</th><td>'+data[j].namesucursal+'</td><td><a href="/tareaBorrarSucursal/'+data[j].id+'" class="btn btn-warning" value="borrarsucursal">Borrar Sucursal</a></td></tr>';
                }
                tablalistado+='</tbody></table>';
                $("#listadoSucursales").html(tablalistado);
          });
        });

        $("#saveTarea").on('click', function(e){
          e.preventDefault();
          var token = $('meta[name="csrf-token"]').attr('content')
          var tareaNueva = $('#tareaNueva').val();
          $.get('tareaNueva/'+tareaNueva, function(data){
            // console.log(data);
            var tablalistado = '<table class="table" id="listado" name="listado"><tbody>'
                for(var j=0; j<data.length; j++){
                    tablalistado+='<tr><th scope="row">'+data[j].id+'</th><td>'+data[j].name+'</td><td><a href="/tareaBorrar/'+data[j].id+'" class="btn btn-warning" value="borrartarea">Borrar Tarea</a></td></tr>';
                }
                tablalistado+='</tbody></table>';
                $("#listado").html(tablalistado);
          });
        });

        $("#borrarSucursal").on('click', function(e){
            e.preventDefault();
            var tr = $(this).closest('tr');
            var idtask = tr.find('th').eq(0).text();
            var nombretask = tr.find('td').eq(0).text();
            alert(nombretask+' a borrar..'); return;
            $.get('tareaBorrar/'+idtask, function(data){
                console.log(data);
                var tablalistado = '<table class="table" id="listado" name="listado"><tbody>'
                    for(var j=0; j<data.length; j++){
                        tablalistado+='<tr><th scope="row">'+data[j].id+'</th><td>'+data[j].name+'</td><td><button type="button" class="btn btn-danger" id="borrarTarea"> Borrar tarea </button></td></tr>';
                    }
                tablalistado+='</tbody></table>';
                $("#listado").html(tablalistado);
            });
        });

        $('.select2-modal').select2({
          minimumResultsForSearch: Infinity,
          dropdownCssClass: 'az-select2-dropdown-modal',
        });

        $('#dateToday').text(moment().format('ddd, MMMM DD YYYY'));



      });

var azCalendarEvents={
         id: 1,
         events:[]
    };

    let token = document.head.querySelector('meta[name="csrf-token"]');
    console.log(token);


    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script defer src="{{asset('js/azia.js')}}"></script>
    <script defer src="{{asset('lib/fullcalendar/fullcalendar.min.js')}}"></script>
    <script defer src="{{asset('lib/select2/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
  var curYear = moment().format('YYYY');
  var curMonth = moment().format('MM');
    var azCalendarEventsUU = {
                      id: 1,
                      events: [
                        {
                          id: '1',
                          start: curYear+'-'+curMonth+'-08T08:30:00',
                          end: curYear+'-'+curMonth+'-08T13:00:00',
                          title: 'julio julio',
                          backgroundColor: '#bff2f2',
                          borderColor: '#00cccc',
                          description: 'prueba julio'
                        },{
                          id: '2',
                          start: curYear+'-'+curMonth+'-10T09:00:00',
                          end: curYear+'-'+curMonth+'-10T17:00:00',
                          title: '22',
                          backgroundColor: '#e0e4f4',
                          borderColor: '#0a2ba5',
                          description: 'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis az pede mollis...'
                        },{
                          id: '3',
                          start: curYear+'-'+curMonth+'-13T12:00:00',
                          end: curYear+'-'+curMonth+'-13T18:00:00',
                          title: 'Lifestyle 3',
                          backgroundColor: '#ffd5cc',
                          borderColor: '#ff5733',
                          description: 'Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi...'
                        },{
                          id: '4',
                          start: curYear+'-'+curMonth+'-15T07:30:00',
                          end: curYear+'-'+curMonth+'-15T15:30:00',
                          title: 'Team Weekly 4',
                          backgroundColor: '#d2e0ff',
                          borderColor: '#0373f3',
                          description: 'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis az pede mollis...'
                        },{
                          id: '5',
                          start: curYear+'-'+curMonth+'-17T10:00:00',
                          end: curYear+'-'+curMonth+'-19T15:00:00',
                          title: 'Music 55',
                          backgroundColor: '#bfdeff',
                          borderColor: '#007bff',
                          description: 'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis az pede mollis...'
                        },{
                          id: '6',
                          start: curYear+'-'+curMonth+'-08T13:00:00',
                          end: curYear+'-'+curMonth+'-08T18:30:00',
                          title: 'Attend Lea\'s 6666777',
                          backgroundColor: '#d5c2f3',
                          borderColor: '#560bd0',
                          description: 'In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis az pede mollis...'
                        }
                      ]
                    };
          var azCalendarEvents={!!$visitas!!}

          $(function(){
  'use strict'


  // Datepicker found in left sidebar of the page
  var highlightedDays = ['2018-5-10','2018-5-11','2018-5-12','2018-5-13','2018-5-14','2018-5-15','2018-5-16'];
  var date = new Date();


  var generateTime = function(element) {
    var n = 0,
    min = 30,
    periods = [' AM', ' PM'],
    times = [],
    hours = [12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

    for (var i = 0; i < hours.length; i++) {
      times.push(hours[i] + ':' + n + n + periods[0]);
      while (n < 60 - min) {
        times.push(hours[i] + ':' + ((n += min) < 10 ? 'O' + n : n) + periods[0])
      }
      n = 0;
    }

    times = times.concat(times.slice(0).map(function(time) {
      return time.replace(periods[0], periods[1])
    }));

    //console.log(times);
    $.each(times, function(index, val){
      $(element).append('<option value="'+val+'">'+val+'</option>');
    });
  }

  generateTime('.az-event-time');

  // Initialize fullCalendar
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listWeek'
    },
    navLinks: true,
    selectable: true,
    selectLongPressDelay: 100,
    editable: true,
    nowIndicator: true,
    defaultView: 'listMonth',
    views: {
      agenda: {
        columnHeaderHtml: function(mom) {
          return '<span>' + mom.format('ddd') + '</span>' +
                 '<span>' + mom.format('DD') + '</span>';
        }
      },
      day: { columnHeader: false },
      listMonth: {
        listDayFormat: 'ddd DD',
        listDayAltFormat: false
      },
      listWeek: {
        listDayFormat: 'ddd DD',
        listDayAltFormat: false
      },
      agendaThreeDay: {
        type: 'agenda',
        duration: { days: 3 },
        titleFormat: 'MMMM YYYY'
      }
    },
    eventSources: [azCalendarEvents],
    eventAfterAllRender: function(view) {
      if(view.name === 'listMonth' || view.name === 'listWeek') {
        var dates = view.el.find('.fc-list-heading-main');
        dates.each(function(){
          var text = $(this).text().split(' ');
          var now = moment().format('DD');

          $(this).html(text[0]+'<span>'+text[1]+'</span>');
          if(now === text[1]) { $(this).addClass('now'); }
        });
      }

      console.log(view.el);
    },
    eventRender: function(event, element) {

      if(event.description) {
        element.find('.fc-list-item-title').append('<span class="fc-desc">' + event.description + '</span>');
        element.find('.fc-content').append('<span class="fc-desc">' + event.description + '</span>');
      }

      var eBorderColor = (event.source.borderColor)? event.source.borderColor : event.borderColor;
      element.find('.fc-list-item-time').css({
        color: eBorderColor,
        borderColor: eBorderColor
      });

      element.find('.fc-list-item-title').css({
        borderColor: eBorderColor
      });

      element.css('borderLeftColor', eBorderColor);
    },
  });

  var azCalendar = $('#calendar').fullCalendar('getCalendar');

  // change view to week when in tablet
  if(window.matchMedia('(min-width: 576px)').matches) {
    azCalendar.changeView('agendaWeek');
  }

  // change view to month when in desktop
  if(window.matchMedia('(min-width: 992px)').matches) {
    azCalendar.changeView('month');
  }

  // change view based in viewport width when resize is detected
  azCalendar.option('windowResize', function(view) {
    if(view.name === 'listWeek') {
      if(window.matchMedia('(min-width: 992px)').matches) {
        azCalendar.changeView('month');
      } else {
        azCalendar.changeView('listWeek');
      }
    }
  });

  // display current date
  var azDateNow = azCalendar.getDate();
  azCalendar.option('select', function(startDate, endDate){
    // $('#modalSetSchedule').modal('show');
    $('#azEventStartDate').val(startDate.format('LL'));
    $('#azEventEndDate').val(endDate.format('LL'));

    $('#azEventStartTime').val(startDate.format('LT')).trigger('change');
    $('#azEventEndTime').val(endDate.format('LT')).trigger('change');
  });



  // Enable/disable calendar events from displaying in calendar

})

</script>
    {{-- <script defer src="{{asset('lib/ionicons/ionicons.js')}}"></script> --}}





    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>


@endsection

