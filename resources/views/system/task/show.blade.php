
@extends('layouts.systema')



@section('content')

  @include('menus.locate_header',['some' => 'Task','location'=>'Home/Task'])

  <div class="row">
    <div class="col-lg-12">
        <h2 class="text-center">Ver Tarea</h2>
    </div>
    <div class="col-lg-12 text-center" style="margin-top:10px;margin-bottom: 10px;">
        <a class="btn btn-primary" href="{{ route('taskIndex') }}"> Atras </a>
    </div>
</div>

<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            <input type="text" name="nombre" value="{{ $task->name }}" class="form-control" placeholder="Nombre">
                        </div>
                    </div>                    
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Estatus:</strong>
                            <select class="browser-default custom-select" name="estado" id="estado">
                                <option selected>Seleccione un Estado ...</option>
                                @foreach ($listStatus as $Status)
                                    <option value="{{$Status->id}}" {{ $Status->id == $task->status? 'selected' : '' }}>{{ $Status->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="{{ $task->id }}" class="form-control">
                </div>
            </div>
            <br><br><br>
        </div>
</div>
@endsection