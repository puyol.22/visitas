@extends('layouts.calendar')


@section('content')
    @include('menus.locate_header',['some' => 'Visitas','location'=>'Home / Visitas / Lista'])
    <div class="az-content az-content-calendar">
      <div class="container">

        <div class="az-content-body az-content-body-calendar">

          <div id="calendar" class="az-calendar"></div>
        </div><!-- az-content-body -->
      </div><!-- container -->
    </div><!-- az-content -->
@endsection

@section('jsInclude')

     <script  src="{{asset('lib/jquery/jquery.min.js')}}"></script>
    <script defer src="{{asset('lib/moment/min/moment.min.js')}}"></script>
    <script defer src="{{asset('lib/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <script defer src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    {{-- <script defer src="{{asset('lib/ionicons/ionicons.js')}}"></script> --}}
    <script defer src="{{asset('lib/fullcalendar/fullcalendar.min.js')}}"></script>
    <script defer src="{{asset('lib/select2/js/select2.full.min.js')}}"></script>

    <script defer src="{{asset('js/azia.js')}}"></script>
    <script  defer src="{{asset('js/app-calendar-events.js')}}"></script>
    <script  defer src="{{asset('js/app-calendar.js')}}"></script>
    <script defer>
      $(function(){
        'use strict'
         $('.az-sidebar .with-sub').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('show');
          $(this).parent().siblings().removeClass('show');
        })

        $(document).on('click touchstart', function(e){
          e.stopPropagation();

          // closing of sidebar menu when clicking outside of it
          if(!$(e.target).closest('.az-header-menu-icon').length) {
            var sidebarTarg = $(e.target).closest('.az-sidebar').length;
            if(!sidebarTarg) {
              $('body').removeClass('az-sidebar-show');
            }
          }
        });


        $('#azSidebarToggle').on('click', function(e){
          e.preventDefault();

          if(window.matchMedia('(min-width: 992px)').matches) {
            $('.az-sidebar').toggle();
          } else {
            $('body').toggleClass('az-sidebar-show');
          }
        })

        $('.select2-modal').select2({
          minimumResultsForSearch: Infinity,
          dropdownCssClass: 'az-select2-dropdown-modal',
        });

        $('#dateToday').text(moment().format('ddd, MMMM DD YYYY'));

      });
    </script>
@endsection
