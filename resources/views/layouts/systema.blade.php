<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Unificados') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <!-- vendor css -->
    <link href="{{ asset('lib/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/typicons.font/typicons.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/morris.js/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/jqvmap/jqvmap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet">

    <!-- azia CSS -->
    <link rel="stylesheet" href="{{ asset('css/azia.css')}}">


  </head>
  <body class="az-body az-body-sidebar az-light" >

    <div  class="az-sidebar">

      <div class="az-sidebar-header">
        <!-- <a href="index.html" class="az-logo">Uni<span>fi</span>cados</a> -->
        <a title="Los Tejos" href=""><img src="{{asset('img/logo.webp')}}" width="130" height="50" alt="Unificados" /></a>
      </div><!-- az-sidebar-header -->
      <div class="az-sidebar-loggedin">
        <!-- <div class="az-img-user online"><img src="https://via.placeholder.com/500" alt=""></div>         -->
        <!-- <div class="media-body">
          <h6>Pedro Perez</h6>
          <span>Supervisor</span>
        </div>media-body -->
      </div><!-- az-sidebar-loggedin -->
      @include('menus.sidebar')
    </div><!-- az-sidebar -->
    <div id="appUnificados" class="az-content az-content-dashboard-five" style="background-color: #fff">
    @include('menus.header')

     @yield('content')

      <div class="az-footer ht-40">
        <div class="container-fluid pd-t-0-f ht-100p">
          <span>&copy; 2019 Unificados Group</span>
        </div><!-- container -->
      </div><!-- az-footer -->
    </div><!-- az-content -->

    @yield('jsInclude')

  </body>
</html>
