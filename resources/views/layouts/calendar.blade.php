<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Sistema visitas unificados group">
    <meta name="author" content="unificados">

       <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Unificados') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
  <!-- https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js -->

    <!-- vendor css -->
    <link href="{{asset('lib/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/typicons.font/typicons.css')}}" rel="stylesheet">
    <link href="{{asset('lib/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">

    <!-- azia CSS -->
    <link rel="stylesheet" href="{{asset('css/azia.css')}}">

  </head>
  <body class="az-body az-body-sidebar az-light">


        <div  class="az-sidebar">

      <div class="az-sidebar-header">
        <!-- <a href="index.html" class="az-logo">Uni<span>fi</span>cados</a> -->
        <a title="Los Tejos" href=""><img src="{{asset('img/logo.webp')}}" width="130" height="50" alt="Unificados" /></a>
      </div><!-- az-sidebar-header -->
      <div class="az-sidebar-loggedin">
      </div><!-- az-sidebar-loggedin -->
      @include('menus.sidebar')
    </div><!-- az-sidebar -->


    <div id="appUnificados" class="az-content az-content-dashboard-five" style="background-color: #fff">
      @include('menus.header')

   @yield('content')

    <div class="az-footer ht-40">
      <div class="container ht-100p pd-t-0-f">
        <span>&copy; 2019 Azia Responsive Bootstrap 4 Dashboard Template</span>
      </div><!-- container -->
    </div><!-- az-footer -->

    <div class="modal az-modal-calendar-schedule" id="modalSetSchedule" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h6 class="modal-title">Create New Event</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div><!-- modal-header -->
          <div class="modal-body">
            <form id="azFormCalendar" method="post" action="calendar.html">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Add title">
              </div><!-- form-group -->
              <div class="form-group d-flex align-items-center">
                <label class="rdiobox mg-r-60">
                  <input type="radio" name="etype" value="event" checked>
                  <span>Event</span>
                </label>
                <label class="rdiobox">
                  <input type="radio" name="etype" value="reminder">
                  <span>Reminder</span>
                </label>
              </div><!-- form-group -->
              <div class="form-group mg-t-30">
                <label class="tx-13 mg-b-5 tx-gray-600">Start Date</label>
                <div class="row row-xs">
                  <div class="col-7">
                    <input id="azEventStartDate" type="text" value="" class="form-control" placeholder="Select date">
                  </div><!-- col-7 -->
                  <div class="col-5">
                    <select id="azEventStartTime" class="select2-modal az-event-time" data-placeholder="Select time">
                      <option label="Select time">Select time</option>
                    </select>
                  </div><!-- col-5 -->
                </div><!-- row -->
              </div><!-- form-group -->
              <div class="form-group">
                <label class="tx-13 mg-b-5 tx-gray-600">End Date</label>
                <div class="row row-xs">
                  <div class="col-7">
                    <input id="azEventEndDate" type="text" value="" class="form-control" placeholder="Select date">
                  </div><!-- col-7 -->
                  <div class="col-5">
                    <select id="azEventEndTime" class="select2-modal az-event-time" data-placeholder="Select time">
                      <option label="Select time">Select time</option>
                    </select>
                  </div><!-- col-5 -->
                </div><!-- row -->
              </div><!-- form-group -->
              <div class="form-group">
                <textarea class="form-control" rows="2" placeholder="Write some description (optional)"></textarea>
              </div><!-- form-group -->

              <div class="d-flex mg-t-15 mg-lg-t-30">
                <button type="submit" class="btn btn-az-primary pd-x-25 mg-r-5">Save</button>
                <a href="" class="btn btn-light" data-dismiss="modal">Discard</a>
              </div>
            </form>
          </div><!-- modal-body -->
        </div><!-- modal-content -->
      </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div class="modal az-modal-calendar-event" id="modalCalendarEvent" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h6 class="event-title"></h6>
            <nav class="nav nav-modal-event">
              <a href="#" class="nav-link"><i class="icon ion-md-open"></i></a>
              <a href="#" class="nav-link"><i class="icon ion-md-trash"></i></a>
              <a href="#" class="nav-link" data-dismiss="modal"><i class="icon ion-md-close"></i></a>
            </nav>
          </div><!-- modal-header -->
          <div class="modal-body">
            <div class="row row-sm">
              <div class="col-sm-6">
                <label class="tx-13 tx-gray-600 mg-b-2">Start Date</label>
                <p class="event-start-date"></p>
              </div>
              <div class="col-sm-6">
                <label class="tx-13 mg-b-2">End Date</label>
                <p class="event-end-date"></p>
              </div><!-- col-6 -->
            </div><!-- row -->

            <label class="tx-13 tx-gray-600 mg-b-2">Description</label>
            <p class="event-desc tx-gray-900 mg-b-30"></p>

            <a href="" class="btn btn-secondary wd-80" data-dismiss="modal">Close</a>
          </div><!-- modal-body -->
        </div><!-- modal-content -->
      </div><!-- modal-dialog -->
    </div><!-- modal -->
  </div>


@yield('jsInclude')

  </body>
</html>
