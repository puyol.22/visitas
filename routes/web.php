<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	$listCompany = App\company::all();
	return view('welcome', ['listCompany ' => $listCompany]);
});

Auth::routes();

Route::get('/home', function () {
	$listCompany = App\company::all();
	$listSucursales = App\branch_office::all();
	$listUsers = App\User::all();
	$listTask = App\task::all();
	$userId = Auth::id();
	$listTaskDay = App\task_day::where('user', '=', $userId)->get();
	return View('home', ['listCompany' => $listCompany,
		'listSucursales' => $listSucursales,
		'listUsers' => $listUsers,
		'listTask' => $listTask,
		'listTaskDay' => $listTaskDay,
		'message' => '',
	]);
})->middleware(['auth']);

//eventList

Route::resource('clients', 'ClientsController')->middleware(['auth']);
Route::resource('sucursales', 'SucursalesController')->middleware(['auth']);
Route::resource('company', 'CompanyController')->middleware(['auth']);
Route::resource('branchOffice', 'BranchOfficeController')->middleware(['auth']);
Route::resource('failedJobs', 'FailedJobsController')->middleware(['auth']);
Route::resource('files', 'FilesController')->middleware(['auth']);
Route::resource('status', 'StatusController')->middleware(['auth']);
Route::resource('task', 'TaskController')->middleware(['auth']);
Route::resource('typeUser', 'TypeUserController')->middleware(['auth']);
Route::resource('visitDetail', 'VisitDetailController')->middleware(['auth']);
Route::resource('visitsPlanning', 'VisitsPlaningController')->middleware(['auth']);
Route::get('/aprobador', 'visitasController@aprobador')->name('aprobador')->middleware(['auth']);
Route::DELETE('/aprobadordestroy/{id}', 'visitasController@borrarVisita')->name('aprobadorborrar')->middleware(['auth']);
Route::get('/siaprobador/{id}', 'visitasController@aprobarVisita')->name('siaprobador')->middleware(['auth']);

//route company form
Route::get('/company', function () {
	$listCompany = App\company::all();
	$list = App\company::all();
	$listStatus = App\statuscompany::all();
	$listUsers = App\User::all();
	$listTask = App\task::all();
	$userId = Auth::id();
	$listTaskDay = App\task_day::where('user', '=', $userId)->get();
	return View('system/company/index', [
		'list' => $list,
		'listStatus' => $listStatus,
		'listCompany' => $listCompany,
		'listUsers' => $listUsers,
		'listTask' => $listTask,
		'listTaskDay' => $listTaskDay,
	]);
})->middleware(['auth'])->name('companyIndex');

Route::post('/company.store', function (Request $request) {
	$company = new App\company;
	$nombre = $_POST['nombre'];
	$estado = $_POST['estado'];
	$rut = $_POST['rut'];
	$telefono = $_POST['telefono'];
	$direccion = $_POST['direccion'];
	$error = 0;

	if (strlen($nombre) < 1) {
		$error = 1;
	}

	if ($estado < 0) {
		$error = 1;
	}

	if ($rut < 1) {
		$error = 1;
	}

	if ($telefono < 1) {
		$error = 1;
	}

	if (strlen($direccion) < 1) {
		$error = 1;
	}

	if ($error == 0) {
		$company->name = $nombre;
		$company->status = $estado;
		$company->rut = $rut;
		$company->address = $direccion;
		$company->phone = $telefono;
		$company->save();

		return response()->json([
			'message' => 'Empresa registrada con exito',
		]);
	} else {
		return response()->json([
			'message' => 'Empresa no pudo ser registrada',
		]);
	}
});

Route::post('/sucursales.store', function (Request $request) {
	$sucursal = new App\sucursales;
	$nombre = $_POST['nombre'];
	$estado = $_POST['estado'];
	$rut = $_POST['rut'];
	$telefono = $_POST['telefono'];
	$direccion = $_POST['direccion'];
	$empresa = $_POST['empresa'];
	$error = 0;

	if (strlen($nombre) < 1) {
		$error = 1;
	}

	if ($estado < 0) {
		$error = 1;
	}

	if ($rut < 1) {
		$error = 1;
	}

	if ($telefono < 1) {
		$error = 1;
	}

	if (strlen($direccion) < 1) {
		$error = 1;
	}

	if ($error == 0) {
		$sucursal->name = $nombre;
		$sucursal->status = $estado;
		$sucursal->rut = $rut;
		$sucursal->address = $direccion;
		$sucursal->phone = $telefono;
		$sucursal->company_id = $empresa;
		$sucursal->save();

		return response()->json([
			'message' => 'Sucursal registrada con exito',
		]);
	} else {
		return response()->json([
			'message' => 'Sucursal no pudo ser registrada',
		]);
	}
});

Route::post('/clients.store', function (Request $request) {
	$usuario = new App\User;
	$nombre = $_POST['nombre'];
	$estado = $_POST['estado'];
	$email = $_POST['email'];

	$error = 0;

	if (strlen($nombre) < 1) {
		$error = 1;
	}

	if ($estado < 0) {
		$error = 1;
	}

	if (strlen($email) < 1) {
		$error = 1;
	}

	if ($error == 0) {
		$usuario->name = $nombre;
		$usuario->status = $estado;
		$usuario->email = $email;
		$usuario->password = '$2y$10$7Bd9ehSpuSC3tJkOOIqsXuQWkedD978mjG.nXokW./RqlndU/URTC';

		$usuario->save();

		return response()->json([
			'message' => 'Usuario registrado con exito',
		]);
	} else {
		return response()->json([
			'message' => 'Usuario no pudo ser registrado',
		]);
	}
});

Route::get('/task.destroy/{id}', 'TaskController@borrarTareaDay')->name('taskDelete');

Route::post('/task.store', function (Request $request) {
	$task = new App\task;
	$nombre = $_POST['nombre'];
	$estado = $_POST['estado'];
	$error = 0;

	if (strlen($nombre) < 1) {
		$error = 1;
	}

	if ($estado < 0) {
		$error = 1;
	}

	if ($error == 0) {
		$task->name = $nombre;
		$task->status = $estado;
		$task->save();

		return response()->json([
			'message' => 'Tarea registrada con exito',
		]);
	} else {
		return response()->json([
			'message' => 'Tarea no pudo ser registrada',
		]);
	}
});

Route::get('sucursalesByCompany/{id}', 'BranchOfficeController@byCompany');

Route::get('tareaNueva/{name}', 'TaskController@nueva');

Route::get('tareaBorrar/{name}', 'TaskController@borrar');

Route::get('/tareaBorrarSucursal/{name}', 'TaskController@borrarSucursal');

Route::post('/taskNueva.store', function (Request $request) {
	$taskday = new App\task_day;
	$id = $_POST['tareaNueva'];

	return response()->json([
		'message' => 'Tarea insertada con exito',
	]);
});

Route::get('sucursalNueva/{id}', 'VisitsPlaningController@nueva');

//route client form
//Route::get('/client', function () {
//	return View('system/client/index');
//})->middleware(['auth'])->name('clientIndex');

//route task form
Route::get('/task', function () {
	$listCompany = App\company::all();
	$list = App\company::all();
	$listStatus = App\statuscompany::all();
	$listUsers = App\User::all();
	$listTask = App\task::all();
	$userId = Auth::id();
	$listTaskDay = App\task_day::where('user', '=', $userId)->get();
	return View('system/task/index', [
		'list' => $list,
		'listStatus' => $listStatus,
		'listCompany' => $listCompany,
		'listUsers' => $listUsers,
		'listTask' => $listTask,
		'listTaskDay' => $listTaskDay,
	]);
})->middleware(['auth'])->name('taskIndex');

Route::get('/event', function () {
	return View('system/event/index');
})->middleware(['auth'])->name('eventIndex');

Route::get('/visitas', 'visitasController@index')->middleware(['auth'])->name('visitsIndex');
Route::post('/visitas', 'visitasController@show')->middleware(['auth']);

Route::get('/visitas/list', function () {
	$listCompany = App\company::all();
	$listSucursales = App\branch_office::all();
	$listUsers = App\User::all();
	$listTask = App\task::all();
	$userId = Auth::id();
	$listTaskDay = App\task_day::where('user', '=', $userId)->get();
	return View('system/visits/list', [
		'listCompany' => $listCompany,
		'listSucursales' => $listSucursales,
		'listUsers' => $listUsers,
		'listTask' => $listTask,
		'listTaskDay' => $listTaskDay,
	]);
})->middleware(['auth'])->name('eventList');
